do_install:append() {
# allow everyone to access mosquitto - also from the outside world
         sed -i -e '$alistener 1883 0.0.0.0\nallow_anonymous true\n' ${D}${sysconfdir}/mosquitto/mosquitto.conf
}
