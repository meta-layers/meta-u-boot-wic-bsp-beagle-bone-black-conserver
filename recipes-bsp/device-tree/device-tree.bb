# Copyright (C) 2022 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Device tree for conserver"
DESCRIPTION = "Device tree for conserver"
SECTION = "bsp"

# the device trees from within the layer are licensed as MIT, kernel includes are GPL
LICENSE = "MIT & GPL-2.0-only"
LIC_FILES_CHKSUM = " \
		file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
		file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6 \
		"

inherit devicetree

FDT_BASE_FILE:beagle-bone-black-conserver = "am335x-boneblack"
FDT_FILE:beagle-bone-black-conserver = "${FDT_BASE_FILE}-1"

# device tree sources for the various machines
COMPATIBLE_MACHINE:beagle-bone-black-conserver = ".*"
SRC_URI:append:beagle-bone-black-conserver = " file://${FDT_FILE}.dts \
 file://am335x-bone-common-1.dtsi \
"

do_install:append() {
    ln -s -r ${D}/boot/devicetree/${FDT_FILE}.dtb ${D}/boot/${FDT_FILE}.dtb
}

FILES:${PN} += "/boot/${FDT_FILE}.dtb"
