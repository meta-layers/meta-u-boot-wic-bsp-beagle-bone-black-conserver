# short-description: Create SD card image with a boot partition
# long-description:
# Create an image that can be written onto an SD card using dd for use
# with beagle-bone-black.
# It uses MLO and u-boot.img
# uEnv.txt is in the rootfs /uEnv.txt
#
# The disk layout used is:
#  ----- --------- --------------
# | MLO | u-boot  |    rootfs    |
#  ----- --------- --------------
# ^     ^         ^              ^
# |     |         |              |
# 128K  384K      4M + rootfs + IMAGE_EXTRA_SPACE
#

# initial uEnv.txt should be on emmc?

# --> raw partitions
part SPL --source rawcopy --sourceparams="file=${SPL_BINARY}" --ondisk mmcblk --no-table --align 128
part u-boot --source rawcopy --sourceparams="file=${UBOOT_BINARY}" --ondisk mmcblk --no-table --align 384
# <-- raw partitions
#part u-boot-env --ondisk ${CONTAINER_WIC_ONDISK} --fstype=vfat --align 4096 --size 1M
# -->
# I made this package which contains a vaild uboot.env
#   oe-pkgdata-util list-pkg-files u-boot-env-helper
#   u-boot-env-helper:
#           /u-boot-env/uboot.env
#
# Like this we can add the u-boot environment /u-boot-env/uboot.env
# in the /u-boot-env fat partition (from where u-boot will pick it up):
# --> /dev/mmcblk0p1
part u-boot-env --source rootfs --rootfs-dir=${IMAGE_ROOTFS}/u-boot-env --ondisk ${CONTAINER_WIC_ONDISK} --fstype=vfat --align 4096 --size 1M
# <-- /dev/mmcblk0p1
# Now u-boot will load my custom u-boot environment from the fat partition
# If we write the partition
# part u-boot-env ... (without the slash in front)
# the partition will not automatically be a mount point in fstab
# <--
#part / --source rootfs --ondisk ${CONTAINER_WIC_ONDISK} --fstype=ext4 --label root --align 8129
# --> /dev/mmcblk0p2
part / --source rootfs --ondisk ${CONTAINER_WIC_ONDISK} --fstype=ext4 --label rootfs1 --align 8129 --fixed-size=10G
# <-- /dev/mmcblk0p2
# --> /dev/mmcblk0p3
part /mnt              --ondisk ${CONTAINER_WIC_ONDISK} --fstype=ext4 --label rootfs2 --align 8129 --fixed-size=10G
# <-- /dev/mmcblk0p3

# --size: The minimum partition size in MBytes. Specify an integer value such as 500. 
#         Do not append the number with “MB”. You do not need this option if you use --source.
#part /var/lib/docker --ondisk mmcblk1 --fstype=btrfs --label data --align 4096 --size 2048
# CONTAINER_ON_PERSISTENT = "1"
# CONTAINER_ON_PERSISTENT_FSTYPE = "btrfs"
# DEVICE_FOR_CONTAINER = "/dev/mmcblk0p3"
# MOUNT_FOR_CONTAINER = "/var/lib/docker"
# CONTAINER_WIC_ONDISK = "mmcblk0" 
# part [mountpoint] Specifying a mntpoint causes the partition to automatically be mounted:
# part ${MOUNT_FOR_CONTAINER} --ondisk ${CONTAINER_WIC_ONDISK} --fstype=${CONTAINER_ON_PERSISTENT_FSTYPE} --label data --align 4096 --size 2048
# If you do not provide mntpoint, Wic creates a partition but does not mount it:
# --> /dev/mmcblk0p5
part container --ondisk ${CONTAINER_WIC_ONDISK} --fstype=${CONTAINER_ON_PERSISTENT_FSTYPE} --label cntdata --align 4096 --size 2048
# <-- /dev/mmcblk0p5
# --> /dev/mmcblk0p6
part /etc-overlay --ondisk ${CONTAINER_WIC_ONDISK} --fstype=ext4 --label etc-overlay --align 8129 --fixed-size=5M
# <-- /dev/mmcblk0p6

# -->
###############################
#
# Y-AB:
# 
# source oe-init-build-env
#
# vim conf/auto.conf
#   MACHINE = "beagle-bone-black"
#
# wic create ../meta-mainline/u-boot-wic-bsp/wic/beagle-bone-black.wks -e core-image-minimal
#
##############################
# bitbake wic-tools
# wic create ../u-boot-wic-bsp/wic/am335x-phytec-wega-sd-card.wks -e core-image-minimal
# wic create ../am335x-phytec-wega-bsp/wic/am335x-phytec-wega-sd-card.wks -e core-image-minimal
# wic create ../meta-mainline/u-boot-wic-bsp/wic/am335x-phytec-wega-sd-card.wks -e core-image-minimal
# INFO: Creating image(s)...
#
# WARNING: bootloader config not specified, using defaults
#
# INFO: The new image(s) can be found here:
#  ./am335x-phytec-wega-sd-card-201706031949-mmcblk.direct
#
#The following build artifacts were used to create the image(s):
#  ROOTFS_DIR:                   /tmp/yocto-autobuilder/yocto-autobuilder/yocto-worker/poky-custom-pyro-multi-v7-core-image-minimal/build/tmp/work/am335x_phytec_wega_bsp-poky-linux-gnueabi/core-image-minimal/1.0-r0/rootfs
#  BOOTIMG_DIR:                  /tmp/yocto-autobuilder/yocto-autobuilder/yocto-worker/poky-custom-pyro-multi-v7-core-image-minimal/build/tmp/work/am335x_phytec_wega_bsp-poky-linux-gnueabi/core-image-minimal/1.0-r0/recipe-sysroot/usr/share
#  KERNEL_DIR:                   /tmp/yocto-autobuilder/yocto-autobuilder/yocto-worker/poky-custom-pyro-multi-v7-core-image-minimal/build/tmp/deploy/images/am335x-phytec-wega-bsp
#  NATIVE_SYSROOT:               /tmp/yocto-autobuilder/yocto-autobuilder/yocto-worker/poky-custom-pyro-multi-v7-core-image-minimal/build/tmp/work/am335x_phytec_wega_bsp-poky-linux-gnueabi/core-image-minimal/1.0-r0/recipe-sysroot-native
#
#INFO: The image(s) were created using OE kickstart file:
#  ../am335x-phytec-wega-bsp/wic/am335x-phytec-wega-sd-card.wks

