FILESEXTRAPATHS:prepend := "${THISDIR}/config:"

# file://features/keyboard-gpio-polled/keyboard-gpio-polled.scc
# file://features/keyboard-gpio/keyboard-gpio.scc

SRC_URI += "file://features/keyboard-gpio-polled/keyboard-gpio-polled.scc \
file://features/uinput/uinput.scc \
file://features/overlayfs/overlayfs.scc \
file://features/usbip/usbip.scc "

# this does not seem to work?
# file://features/usb-winchiphead/usb-winchiphead.scc
