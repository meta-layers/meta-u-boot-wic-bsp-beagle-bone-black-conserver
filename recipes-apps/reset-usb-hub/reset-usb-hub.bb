# Copyright (C) 2024 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "reset usb hub"
DESCRIPTION = "reset usb hub"
SECTION = "bsp"

LICENSE = "MIT"
LIC_FILES_CHKSUM = " \
		file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
		"

SRC_URI = "file://reset-usb-hub.sh"

do_compile(){
}

do_install() {
        # shell script, which does the work
        install -d ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/reset-usb-hub.sh ${D}${bindir}
}

