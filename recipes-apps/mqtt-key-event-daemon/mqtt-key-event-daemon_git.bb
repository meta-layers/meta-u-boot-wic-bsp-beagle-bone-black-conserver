DESCRIPTION = "C/cmake mqtt-key-event-daemon which uses libpaho and libgpiod"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

#SRCREV = "${AUTOREV}"
SRCREV = "4c86f65a54c12d8d247e94d4aaf3577db7fcb364"
SRC_URI = "git://gitlab.com/exempli-gratia/mqtt-key-event-daemon;protocol=https;branch=master"
# Note: this only works with PR_server running!
# PV is expanded to something like 1.0+gitAUTOINC+72f0bb27a5"
# The PR_server? replaces AUTOINC with a number in the package name
# use-libpalindrome-cpp_1.0+git0+72f0bb27a5-r0.4:armv7at2hf-neon.ipk
PV = "1.0+git${SRCPV}"

inherit cmake pkgconfig

S = "${WORKDIR}/git/src"

DEPENDS = "libgpiod paho-mqtt-c zlog"
