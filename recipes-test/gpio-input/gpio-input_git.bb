DESCRIPTION = "C/cmake which uses libgpiod"
SECTION = "examples"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

#SRCREV = "${AUTOREV}"
SRCREV = "d5d6a3b9235a556f9d06e486d068311c300a4a89"
SRC_URI = "git://gitlab.com/exempli-gratia/gpio-input;protocol=https;branch=master"
# Note: this only works with PR_server running!
# PV is expanded to something like 1.0+gitAUTOINC+72f0bb27a5"
# The PR_server? replaces AUTOINC with a number in the package name
# use-libpalindrome-cpp_1.0+git0+72f0bb27a5-r0.4:armv7at2hf-neon.ipk
PV = "1.0+git${SRCPV}"

inherit cmake

S = "${WORKDIR}/git"

DEPENDS = "pkgconfig-native libgpiod"
