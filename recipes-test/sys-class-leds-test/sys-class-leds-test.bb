# Copyright (C) 2022 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "/sys/class/leds test"
DESCRIPTION = "/sys/class/leds test"
SECTION = "bsp"

LICENSE = "MIT"
LIC_FILES_CHKSUM = " \
		file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
		"

SRC_URI = "file://sys-class-leds-test.sh"

do_compile(){
}

do_install() {
        # shell script, which does the work
        install -d ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/sys-class-leds-test.sh ${D}${bindir}
}

