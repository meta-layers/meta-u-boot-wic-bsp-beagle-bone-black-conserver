#!/bin/sh

sleep 3

echo 1 > /sys/class/leds/usbswitch:not-usb-power/brightness
echo 1 > /sys/class/leds/usbswitch:not-usb-cable/brightness

for LED in usbswitch:usbout0 usbswitch:usbout1 usbswitch:usbout2 usbswitch:usbout3 usbswitch:usbout4 usbswitch:usbout5 usbswitch:usbout6
do
    # toggle 
    echo 1 > /sys/class/leds/${LED}/brightness
    sleep 1
    echo 0 > /sys/class/leds/${LED}/brightness

    # toggle
    echo 1 > /sys/class/leds/${LED}/brightness
    sleep 0.5
    echo 0 > /sys/class/leds/${LED}/brightness
done

