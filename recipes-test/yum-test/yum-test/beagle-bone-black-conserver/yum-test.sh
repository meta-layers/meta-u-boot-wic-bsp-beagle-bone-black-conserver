#!/bin/sh
sleep 3
for LED in usbswitch:red0 usbswitch:green0 usbswitch:red1 usbswitch:green1 usbswitch:red2 usbswitch:green2 usbswitch:red3 usbswitch:green3 usbswitch:red4 usbswitch:green4 usbswitch:red5 usbswitch:green5 usbswitch:red6 usbswitch:green6
do
    echo 1 > /sys/class/leds/${LED}/brightness
    sleep 0.5
    echo 0 > /sys/class/leds/${LED}/brightness
    sleep 0.3
done

# not-usb-power   not-usb-cable
# =================================================================================================================================
# active          active          this is the default - how the system boots
#                                 e.g.
#                                 [   18.822780] usb 1-1.1.1: new full-speed USB device number 4 using musb-hdrc
#                                 [   19.036781] ftdi_sio 1-1.1.1:1.0: FTDI USB Serial Device converter detected
#                                 [   19.044142] usb 1-1.1.1: Detected FT232RL
#                                 [   19.137749] usb 1-1.1.1: FTDI USB Serial Device converter now attached to ttyUSB0
# active          inactive        it's like disconnecting the active USB cables from the Atolla
#                                 e.g.
#                                 [  443.374698] usb 1-1.1: USB disconnect, device number 3
#                                 [  443.379943] usb 1-1.1.1: USB disconnect, device number 4
#                                 [  443.394066] ftdi_sio ttyUSB0: FTDI USB Serial Device converter now disconnected from ttyUSB0
#                                 [  443.402995] ftdi_sio 1-1.1.1:1.0: device disconnected
# inactive        active          it's like connecting the active USB cables from the Atolla
#                                 e.g.
#                                 [   18.822780] usb 1-1.1.1: new full-speed USB device number 4 using musb-hdrc
#                                 [   19.036781] ftdi_sio 1-1.1.1:1.0: FTDI USB Serial Device converter detected
#                                 [   19.044142] usb 1-1.1.1: Detected FT232RL
#                                 [   19.137749] usb 1-1.1.1: FTDI USB Serial Device converter now attached to ttyUSB0
# inactive        inactive        this is like disconnecting the USB cable and the power from the Atolla USB hub
# =================================================================================================================================

# both inactive, this is like disconnecting the USB cable and the power from the Atolla USB hub
echo 0 > /sys/class/leds/usbswitch:not-usb-power/brightness
echo 0 > /sys/class/leds/usbswitch:not-usb-cable/brightness
echo "+++ both inactive, this is like disconnecting the USB cable and the power from the Atolla USB hub"
sleep 2

# both active, this is how the system boots
echo 1 > /sys/class/leds/usbswitch:not-usb-power/brightness
echo 1 > /sys/class/leds/usbswitch:not-usb-cable/brightness
echo "+++ both active, this is how the system boots"
sleep 2

# it's like disconnecting the active USB cables from the Atolla
echo 1 > /sys/class/leds/usbswitch:not-usb-power/brightness
echo 0 > /sys/class/leds/usbswitch:not-usb-cable/brightness
echo "+++ it's like disconnecting the active USB cables from the Atolla"
sleep 2

# it's like connecting the active USB cables from the Atolla
echo 0 > /sys/class/leds/usbswitch:not-usb-power/brightness
echo 1 > /sys/class/leds/usbswitch:not-usb-cable/brightness
echo "+++ it's like connecting the active USB cables from the Atolla"
sleep 2

# both active, this is how the system boots
echo 1 > /sys/class/leds/usbswitch:not-usb-power/brightness
echo 1 > /sys/class/leds/usbswitch:not-usb-cable/brightness
echo "+++ both active, this is how the system boots"
sleep 2

for LED in usbswitch:usbout0 usbswitch:usbout1 usbswitch:usbout2 usbswitch:usbout3 usbswitch:usbout4 usbswitch:usbout5 usbswitch:usbout6
do
    # toggle 
    echo 1 > /sys/class/leds/${LED}/brightness
    sleep 1
    echo 0 > /sys/class/leds/${LED}/brightness

    # toggle
    echo 1 > /sys/class/leds/${LED}/brightness
    sleep 0.5
    echo 0 > /sys/class/leds/${LED}/brightness
done

