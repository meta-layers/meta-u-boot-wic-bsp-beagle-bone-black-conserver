# Copyright (C) 2022 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "/sys/class/leds test"
DESCRIPTION = "/sys/class/leds test"
SECTION = "bsp"

LICENSE = "MIT"
LIC_FILES_CHKSUM = " \
		file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
		"

SRC_URI = "file://yum-test-3led.sh \
           file://yum-test-led-rg.sh \
           file://yum-test.sh \
           file://yum-test-usb.sh \
"

do_compile(){
}

do_install() {
        # shell script, which does the work
        install -d ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/yum-test-3led.sh ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/yum-test-led-rg.sh ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/yum-test.sh ${D}${bindir}
        install -m 0755 ${UNPACKDIR}/yum-test-usb.sh ${D}${bindir}
}
